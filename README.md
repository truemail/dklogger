# dk_logger

A logger for **Digital Kites** processes.

## Installation

Make sure you have [Node.js](http://nodejs.org/) and [Npm](https://www.npmjs.com/) installed.

```sh
npm install dk-logger@latest
```

This will be added to your package.json dependencies.

## Usage

```js
const dk_logger = require('dk-logger');

dk_logger.setApiKey('YOUR API KEY');
dk_logger.setErrorReportObj({
  projectId: 'PROJECT ID', // required
  credentialsPath: 'PROJECT CREDENTIALS FILE PATH' // optional
});

const obj = {
    app_ip:"x.x.x.x",  // optional 
    app_name:"your pm2 app name",  // optional
    app_id: 1, // pm2 process id (optional)
    date: Math.floor(new Date().setHours(0, 0, 0, 0) / 1000), // your date epoch in seconds
    time: Math.floor(Date.now() / 1000), // your time epoch in seconds
    message: "some message",//your message
    status: "online",
    log_level:"info" //["fatal", "error", "warn","info","debug"]
}

dk_logger.save(obj);
```

#### How to include in your process:

```js
//Note:Must include this in the first line of your file(which is started with pm2).
//Note:Make sure this snippet runs in production envirnoment.
//NOTE: custom error messages(other than errors originated) should be passed as --> message:new Error(YOUR ERROR), all other errors as --> message: YOUR ERROR 
const dk_logger = require('dk-logger');

dk_logger.setApiKey('YOUR API KEY');
//To send error logs to Google
dk_logger.setErrorReportObj({
  projectId: 'PROJECT ID', // required
  credentialsPath: 'PROJECT CREDENTIALS FILE PATH' // optional
});

process.on('uncaughtException', function(e) {
  console.error('[uncaughtException] app will be terminated: ', e);
  const obj = {
    date: Math.floor(new Date().setHours(0, 0, 0, 0) / 1000), // your date epoch in seconds
    time: Math.floor(Date.now() / 1000), // your time epoch in seconds
    message: e.message,
    status: "stopped",
    log_level:"error"
  }
  dk_logger.save(obj).then(function(){
    process.exit(0);
    throw new Error(e.message);
  });
});
```
