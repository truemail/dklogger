const process = require("process");
var async = require("async");
var pm2 = require("pm2");
const reportError = require("./googleReportError");
const db = require('./localDB');
const fs = require('fs');
const fetch = require('node-fetch');

var options = {
  method: "GET",
  uri: "https://ipv4.icanhazip.com/"
  // json: true // Automatically stringifies the body to JSON
};

let api_url = "http://api.congolop.com/api/saveRecord";
const logsPath = './ErrorLogsDkLogger.json'
class dkLogger {
  constructor(API_URL=null) {
    if(API_URL) api_url = API_URL;
    this.setHourEpoch();
    this.reportError = new reportError()
    this.user = new db(logsPath);
  }
  getProcessDetails() {
    return new Promise((resolve, reject) => {
      pm2.connect(function(err) {
        // console.log(err);
        pm2.list(function(err, list) {
          let process_name;
          let process_id;
          async.forEach(
            list,
            function(doc, callback) {
              if (process.pid == doc.pid) {
                // console.log("name", doc.name, doc.pm_id,doc.pm2_env.status);
                process_name = doc.name;
                process_id = doc.pm_id;
                callback("errrr");
              } else {
                callback(null, process);
              }
            },
            function(err) {
              const obj = { process_name, process_id };
              resolve(obj);
              pm2.disconnect(function(err, doc) {});
            }
          );
          // pm2.disconnect(function(err,doc){console.log(err,doc)})
        });
      });
    });
  }

  setApiKey(key) {
    this.apiKey = key;
  }

  setErrorReportObj(obj={}){
    if(typeof(obj) != 'object' || !obj.projectId || typeof(obj.projectId)!='string') return
    const status = this.reportError.setErrorReportObj(obj);
    if(status) this.sucessInstatiation = true
  }

  async save(insertObj = {}) {
    try{
      await this.saveToGoogle(insertObj)
      console.log("error reported successfully");
    }catch(err){
      console.log('saveToGoogle Error:\n',err)
    }
    if (!this.apiKey)
      return console.error({
        status: "Error",
        message: "Must set Api Key."
      });
    return new Promise(
      function(resolve, reject) {
        try {
          let apiKey = this.apiKey;
          this.getProcessDetails().then(function(detailsObj) {
            if (!typeof insertObj == "object")
              throw "Must pass object to the save method";
            const infoObj = {
              title: process.title,
              pid: process.pid,
              platform: process.platform,
              id: process.mainModule.id,
              filename: process.mainModule.filename
            };
            insertObj["app_name"] = detailsObj.process_name
              ? detailsObj.process_name
              : infoObj.filename;
            if (detailsObj.process_id == 0)
              insertObj["app_id"] = detailsObj.process_id;
            else {
              insertObj["app_id"] = detailsObj.process_id
                ? detailsObj.process_id
                : -1;
            }
            insertObj["info"] = infoObj;
            fetch(api_url,{
              method:'POST',
              body: JSON.stringify(insertObj) ,
              headers: {
                'Content-Type': 'application/json',
                access_token: apiKey
               },        
              })
              .then(res=>res.json())
              .then( 
                data=>{
                console.log("body:", data);
                resolve()              
                },
                err=>{
                console.error('error:', err)
                resolve();
                }
              )
           });
        } catch (err) {
          console.error({
            status: "Internal Error",
            message: JSON.stringify(err)
          });
          resolve();
        }
      }.bind(this)
    );
  }

  getIp(){
    return new Promise((resolve,reject)=>{
      fetch(options.uri,{
        method:options.method,
      })
      .then(res => res.text())
      .then(
        data=>{ 
          console.log("getIp:",data)
          resolve(data)
        },
        err=>{
          console.error('errorIp:', err)
          resolve(null);
        }
      )
    })
  }

  setHourEpoch() {
    this.currentHourEpoch = Math.floor(
      new Date().setHours(new Date().getHours(), new Date().getMinutes(), 0, 0) / 1000
    );
    this.endHourEpoch = Math.floor(new Date().setHours(new Date().getHours(), new Date().getMinutes() + 5, 0, 0) / 1000)
  }

  saveToGoogle(insertObj = {}) {
    if(!this.sucessInstatiation) return console.error({
      status:"Error",
      message:'GoogleReportError instatiation failed.'
    });
    return new Promise(
      (resolve, reject)=>{
        try {
          let apiKey = this.apiKey;
          this.getProcessDetails().then(async detailsObj => {
            if (!typeof insertObj == "object")
              throw "Must pass object to the save method";
            const infoObj = {
              title: process.title,
              pid: process.pid,
              platform: process.platform,
              id: process.mainModule.id,
              filename: process.mainModule.filename
            };
            insertObj["app_name"] = detailsObj.process_name
              ? detailsObj.process_name
              : infoObj.filename;
            if (detailsObj.process_id == 0)
              insertObj["app_id"] = detailsObj.process_id;
            else {
              insertObj["app_id"] = detailsObj.process_id
                ? detailsObj.process_id
                : -1;
            }
            insertObj["info"] = infoObj;
            var options = {
              method: "POST",
              headers: {
                access_token: apiKey
              },
              uri: api_url,
              body: insertObj,
              json: true // Automatically stringifies the body to JSON
            };
            this.ErrorObject = {
              processName: insertObj.app_name,
              message: "",
              ip: "",
              filePath: "",
              lineNumber: "",
              functionName: "",
              process_id:insertObj.app_id
            };
            if(!insertObj.message) return console.log("your error must be in message property.")
              if (insertObj.message.stack) {
                const splitStackTrace = insertObj.message.stack.split("\n");
                const splitErrorMessage = splitStackTrace[0].split(":");
                let errorFileWithLine = splitStackTrace[1];
                let index = errorFileWithLine.indexOf("(");
                let clean = errorFileWithLine.slice(
                  index + 1,
                  errorFileWithLine.length - 1
                );
                let split = clean.split(":");
                this.ErrorObject.filePath = split[0];
                this.ErrorObject.lineNumber = split[1];
                this.ErrorObject.columnNumber = split[2];
                this.ErrorObject.ErrorType = (splitErrorMessage[0]) ? splitErrorMessage[0].trim():"";
                this.ErrorObject.message = (splitErrorMessage[1]) ? splitErrorMessage[1].trim():"";
              } else {
                this.ErrorObject.ErrorType = "Error";
                // this.ErrorObject.message = JSON.stringify(insertObj.message);
              }
            let message;
            if (!(insertObj.message instanceof Error)) {
              if(typeof insertObj.message == 'object'){
                message = new Error(JSON.stringify(insertObj.message));
              }else message = new Error(insertObj.message);
            } else {
              message = insertObj.message;
            }
            let myIp = await this.getIp()
            if(myIp) myIp = myIp.replace(/\s+/,'')
            // console.log('ip:',myIp)
            // return
            this.ErrorObject.ip = insertObj.app_ip
              ? insertObj.app_ip
              : myIp;
            message.name = `${myIp} - ${insertObj.app_name} - ${
              insertObj.app_id
            } - ${message.name}`;
            // return
            this.ErrorObject.message = message;

            this.ErrorObject.filePath = (this.ErrorObject.filePath)?this.ErrorObject.filePath : insertObj.filePath;
            const currentTime = Math.floor(Date.now() / 1000);
            /*
            console.log(
              `currentTime:${new Date(
                currentTime * 1000
              ).toLocaleString()} currentHour:${new Date(
                this.currentHourEpoch * 1000
              ).toLocaleString()} endHour:${new Date(
                this.endHourEpoch * 1000
              ).toLocaleString()}`
            );*/
            if (currentTime >= this.endHourEpoch) {
              try{
                let removedCount = await this.user.remove(this.currentHourEpoch);
                // console.log("removed Count:",removedCount)
              }catch(err){
                console.log("dk-local remove error",err)
              }
              // console.log("setting epoch...");
              this.setHourEpoch();
              // console.log("epoch set");
            }
            const filter = {
              app_ip: this.ErrorObject.ip,
              app_name: this.ErrorObject.processName,
              app_id: this.ErrorObject.process_id,
              message: this.ErrorObject.message.message
            };
            try{
              let count = await this.user.find(filter,this.currentHourEpoch,this.endHourEpoch);
              // console.log(count)
              if(count >= 3){
                // console.log("too many requests.")
                return
              }
              const insertObj = {
                app_ip: this.ErrorObject.ip,
                app_name: this.ErrorObject.processName,
                app_id: this.ErrorObject.process_id,
                // log_level: insertObj.log_level,
                // status: insertObj.status,
                message: this.ErrorObject.message.message,
              };
              await this.user.insertOne(insertObj)
            }catch(err){
              console.log("errorr",err)
            }
            
            try{
              // console.log('sending report')
              await this.reportError.reportError(this.ErrorObject);
              resolve()
            }catch(err){
              console.log(err)
              return reject(err)
            }
          });
        } catch (err) {
          console.error({
            status: "Internal Errorr",
            message: JSON.stringify(err)
          });
          reject()
        }
      }
    );
  }
}

module.exports = new dkLogger();
