const {ErrorReporting} = require('@google-cloud/error-reporting');
// Instantiates a client
class GoogleErrorReport{
  constructor(){
  }
  setErrorReportObj(config={}){
    try{
      this.errors = new ErrorReporting({
        projectId : config.projectId,
        // keyFilename: config.filePath ? config.filePath : path.join(__dirname,'errorReportCredentials.json'),
        keyFilename: config.credentialsPath ? config.credentialsPath : '/home/harsha/errorReportCredentials.json',
      });
      return 1
    }catch(err){
      console.log("ErrorInstatiation:\n",err)
      return 0
    }
  }

  reportError(errObj = {}){
    // console.log("errObj",errObj);
      return new Promise(async(resolve,reject)=>{
          try{
              const errorEvent = this.errors.event();
              let index = (errObj.message.stack).indexOf(":")
              errorEvent.setMessage(`${(errObj.message.name)} ${errObj.message.stack.slice(index+1,)}`);
              errorEvent.setFilePath(errObj.filePath);
              errorEvent.setLineNumber(parseInt(errObj.lineNumber));
              // errorEvent.setFunctionName("errObj.functionName");
              errorEvent.setServiceContext(errObj.processName,errObj.ip);
              this.errors.report(errorEvent, (err,success) => {
                  if(err){
                    // console.log('error',JSON.stringify(err),success)
                    return reject(JSON.stringify(err))
                  }
                  // console.log('Report Saved to Google.success')
                  resolve()
              });
          }catch(err){
              // console.error('ERROR:\n',JSON.stringify(err))
              this.errors.report(err, (err,success) => {
                if(err){
                  // console.log('error',JSON.stringify(err),success)
                  return reject(JSON.stringify(err))
                }
                // console.log('Report Saved to Google')
                resolve()
              });
          }
      })
  }
}



module.exports = GoogleErrorReport

//   process.on('uncaughtException', err => {
//     err.name='209.dsa '+err.name;
//   });

// Reports a simple error
//errors.report('Something broke!');
// Report the error event




//errors.report(), () => console.log('done!'));




// errors.report(errorEvent, () => {
//   console.log('Done reporting error event!');
// });
/*
// Report an Error object
errors.report(new Error('My error message'), () => {
  console.log('Done reporting Error object!');
});

// Report an error by provided just a string
errors.report('My error message', () => {
  console.log('Done reporting error string!');
});
*/