var fs = require('fs');
class LocalDB{
    constructor(error_logs_path){
        this.logsPath = error_logs_path;
    }

    insertOne(obj){
        const insertTimes = {
            insertedTime: Math.floor(Date.now() / 1000),
            insertedDate: Math.floor(new Date().setHours(0, 0, 0, 0) / 1000)
        }
        const data = this.fetchAll()

        const write_data = Object.assign(obj,insertTimes);
        data.push(write_data);
        fs.writeFileSync(this.logsPath,JSON.stringify(data,null,2))
        console.log('inserted')
    }

    fetchAll(){
        let data = [];
        if(!fs.existsSync(this.logsPath)){
          fs.writeFileSync(this.logsPath, '[]', {encoding: 'utf8'});
        }else{
          const json = fs.readFileSync(this.logsPath, 'utf8');
          data = JSON.parse(json);
        }
        return data;
    }

    find(filter,currentHourEpoch,endHourEpoch){
        const data = this.fetchAll();
        // console.log('data',typeof(data),data)
        if(data.length == 0) return 0;
        const newArray = data.filter((errObj)=>{
            return ( errObj.insertedTime>=currentHourEpoch && errObj.insertedTime < endHourEpoch) &&
            ( filter.app_ip == errObj.app_ip && filter.app_name == errObj.app_name &&
              filter.app_id == errObj.app_id && filter.message == errObj.message)
        });
        return newArray.length
    }
    remove(currentHourEpoch){
        const data = this.fetchAll();
        // console.log('datalength:',data.length)
        if(data.length == 0) return resolve(0)
        const newArray = []
        for(let errObj of data){
          if(errObj.insertedTime >= currentHourEpoch){
            newArray.push(errObj)
          }
        }
        let removed = Math.abs(data.length - newArray.length)
        fs.writeFileSync(this.logsPath, JSON.stringify(newArray,null,2));
        // console.log('removed')
        return removed;
    }
}

module.exports = LocalDB;